export const data = [
    {
        id: 1,
        img:'https://upload.wikimedia.org/wikipedia/commons/thumb/1/1e/Erasmus%2B_Logo.svg/2560px-Erasmus%2B_Logo.svg.png'
    },
    {
        id: 2,
        img: 'https://fintech.id/storage/files/shares/logo/logofi2/amartha-logo-2020.png'
    },
    {
        id: 3,
        img: 'https://upload.wikimedia.org/wikipedia/commons/1/15/Logo_Kementerian_Perhubungan_Indonesia_%28Kemenhub%29.png'
    },
    {
        id: 4,
        img: 'https://nfc.or.id/wp-content/uploads/2022/09/download.png'
    },
    {
        id: 5,
        img: 'https://upload.wikimedia.org/wikipedia/id/thumb/5/55/BNI_logo.svg/2560px-BNI_logo.svg.png'
    },
    {
        id: 6,
        img: 'https://upload.wikimedia.org/wikipedia/commons/thumb/a/ad/Bank_Mandiri_logo_2016.svg/1200px-Bank_Mandiri_logo_2016.svg.png'
    },
    {
        id: 7,
        img:'https://upload.wikimedia.org/wikipedia/commons/thumb/6/68/BANK_BRI_logo.svg/1280px-BANK_BRI_logo.svg.png'
    },
    {
        id: 8,
        img:'https://images.bisnis.com/posts/2022/03/17/1511871/centratama-group.jpg'
    },
    {
        id: 9,
        img:'https://registerevent.prasetiyamulya.ac.id/reg-cv/theme/default/images/logo_pm.png'
    },
    {
        id: 10,
        img: 'https://images.glints.com/unsafe/glints-dashboard.s3.amazonaws.com/company-logo/56e83c6db8cd5587e87161281dfba75b.png'
    },
    {
        id: 11,
        img:'https://www.erajaya.com/files/uploads/newseventattachment/uri/2021/Sep/21/61494d348d879/logo-subsidiaries-maret2020-erajaya-1091x_.png?token=3d2477ed33af3834f6388585f8eb9051'
    },
    {
        id: 12,
        img:'https://media.giphy.com/avatars/20FITindonesia/pJzAchsQwMJT.png'
    },
]