import { useState } from "react";
import { Link } from "react-scroll";
import {FaTimes} from "react-icons/fa";
import {CiMenuFries} from "react-icons/ci";
import siesta from "../assets/img/siesta.png";

const Nav = () => {
  const [click, setClick] = useState(false);
  const handleClick = () => setClick(!click);
  const content = (
    <>
      <div className="lg:hidden block absolute top-16 w-full left-0 right-0 bg-white transition ">
        <ul className="text-center text-xl p-20">
          <Link  spy={true} smooth={true} to="Home">
            <li className="my-4 py-4 border-b border-slate-800 hover:bg-white hover:rounded">Home</li>
          </Link>
          <Link spy={true} smooth={true} to="About">
            <li className="my-4 py-4 border-b border-slate-800 hover:bg-white hover:rounded">Our Works</li>
          </Link>
          <Link spy={true} smooth={true} to="Services">
            <li className="my-4 py-4 border-b border-slate-800 hover:bg-white hover:rounded">Our Products</li>
          </Link>
          <Link spy={true} smooth={true} to="Projects">
            <li className="my-4 py-4 border-b border-slate-800 hover:bg-white hover:rounded">Our Service</li>
          </Link>
          <Link spy={true} smooth={true} to="Contact">
            <li className="my-4 py-4 border-b border-slate-800 hover:bg-white hover:rounded">Contact Us</li>
          </Link>
        </ul>
      </div>
    </>
  );

  return (
    <nav>
      <div className="h-10vh w-full flex justify-between z-50 text-[#265BE1] lg:pb-0 lg:pt-7 px-20 py-4">
        <div className="flex items-center flex-1">
          <img className="w-44" src={siesta} alt="/" />
        </div>
        <div className="lg:flex md:flex lg:flex-1 items-center justify-end font-normal hidden">
          <div className="flex-10 w-full">
            <ul className="flex gap-8 mr-16 w-full text-[18px]">
              <Link spy={true} smooth={true} to="Home">
                <li className="transition border-b-2 lg:text-xl   border-white hover:border-[#1624A3] cursor-pointer">Home</li>
              </Link>
              <Link spy={true} smooth={true} to="About">
                <li className="transition border-b-2 w-22  lg:text-xl border-white hover:border-[#1624A3] cursor-pointer">Our Works</li>
              </Link>
              <Link spy={true} smooth={true} to="Services">
                <li className="transition border-b-2 w-22 lg:text-xl border-white hover:border-[#1624A3] cursor-pointer">Our Products</li>
              </Link>
              <Link spy={true} smooth={true} to="Project">
                <li className="transition border-b-2 w-22 lg:text-xl border-white hover:border-[#1624A3] cursor-pointer">Our Service</li>
              </Link>
              <Link spy={true} smooth={true} to="Contact">
                <li className="transition font-bold  text-center lg:text-xl text-white bg-[#1624A3] p-[1px]  px-6 rounded-md h-[32px] border-white  cursor-pointer">Contact Us</li>
              </Link>
            </ul>
          </div>
        </div>
        <div>
            {click && content}
        </div>
        
        <button className="block sm:hidden transtion" onClick={handleClick}>
          {click ? <FaTimes/> : <CiMenuFries /> }
        </button>
      </div>
    </nav>
  );
};

export default Nav;
