/* eslint-disable react/no-unescaped-entities */
/* eslint-disable react/jsx-key */
/* eslint-disable no-unused-vars */
import Nav from "./Components/Nav";
import { data } from "./mockData.js";
import { MdChevronLeft, MdChevronRight } from "react-icons/md";
import s1 from "./assets/img/s1.png";
import s2 from "./assets/img/s2.png";
import s3 from "./assets/img/s3.png";
import s4 from "./assets/img/s4.png";
import s5 from "./assets/img/s5.png";
import vector1 from "./assets/img/Vector1.png";
import vector2 from "./assets/img/Vector2.png";
import vector3 from "./assets/img/Vector3.png";
import prop from "./assets/img/siesta3.png";
import prop1 from "./assets/img/siesta2.png";
import AlyasinQu from "./assets/img/AlyasinQu.png";
import Retailqu from "./assets/img/siesta5.png";
import Campusqu from "./assets/img/CampusQu.png";

function App() {
  const slideLeft = () => {
    var slider = document.getElementById("slider");
    slider.scrollLeft = slider.scrollLeft - 500;
  };

  const slideRight = () => {
    var slider = document.getElementById("slider");
    slider.scrollLeft = slider.scrollLeft + 500;
  };

  return (
    <>
      <div className="w-full h-full">
        <Nav />
        <div className="bg-[url(./assets/img/desk2.png)] w-full bg-cover h-[550px]">
          <h1 className="text-[46px] text-white mt-5 ml-7 text-left lg:text-[53px] sm:mt-10 sm:ml-[150px] sm:text-left w-[550px] sm:w-[700px]">
            <b>
              Siesta is exactly your preferred and reliable tech consulting
              company
            </b>
          </h1>
          <h2 className=" ml-9 text-justify text-white w-[440px] mt-3 text-[20px] sm:ml-[150px] sm:text-2xl sm:w-[740px]">
          <b>
            We will help you materialize your vision and make your digital
            transformation as easy as breeze.
          </b>
        </h2>
        </div>

        <h1 className="text-center mt-5 text-[#0082C9] sm:text-lg text-sm">
          <b>
            WE ARE THE PEOPLE BEHIND MANY SUCCESSFUL BANKS, FINTECHS &
            E-COMMERCE STARTUP PLATFORMS, INCLUDING GOVERNMENT
          </b>
        </h1>

        <div className="relative flex items-center ">
          <MdChevronLeft
            className="opacity-50 cursor-pointer hover:opacity-100"
            onClick={slideLeft}
            size={40}
          />
          <div
            id="slider"
            className="w-full h-full overflow-x-scroll scroll whitespace-nowrap scroll-smooth scrollbar-hide"
          >
            {data.map((item) => (
              <img
                className="w-[220px] inline-block p-16 cursor-pointer hover:scale-105 ease-in-out duration-300"
                src={item.img}
                alt="/"
              />
            ))}
          </div>
          <MdChevronRight
            className="opacity-50 cursor-pointer hover:opacity-100"
            onClick={slideRight}
            size={40}
          />
        </div>
      </div>
      <div className="w-full h-full">
        <h2 className="text-xl mb-3 text-[#0082C9] text-center">
          <b>Our Service</b>
        </h2>
        <div className="lg:w-[1200px] w-[500px] lg:mx-auto ml-28  h-full flex lg:justify-around flex-col lg:flex-row lg:flex-wrap">
          <div>
            <img
              className="absolute ml-5 lg:ml-16 object-cover mt-6 lg:mt-5 lg:w-[70px] lg:h-[55px]"
              src={s1}
              alt="/"
            />
          </div>
          <div className="w-[351px] h-[91px] bg-white rounded-xl  shadow-md flex items-center">
            <h1 className="lg:text-[20px] text-[19px] font-bold text-center mx-auto mr-9 text-[#1624A3]">
              IT Consulting
            </h1>
          </div>
          <div>
            <img
              className="lg:w-[70px] w-[50px] h-[50px] ml-5 mt-14 lg:mt-4 lg:h-[65px] absolute lg:ml-7  object-cover"
              src={s2}
              alt=""
            />
          </div>
          <div className=" w-[371px] lg:w-[470px] h-[91px] bg-white rounded-xl mt-10 lg:mt-0  shadow-md flex items-center">
            <h1 className="text-[19px] lg:text-[20px] font-bold text-center mx-auto mr-6 text-[#1624A3] ">
              Authorized Gold Distributor
            </h1>
          </div>
          <div>
            <img
              className="absolute lg:w-[70px] lg:h-[65px] mt-16 lg:mt-4 ml-6 object-cover"
              src={s3}
              alt=""
            />
          </div>
          <div className="w-[351px] h-[91px] bg-white rounded-xl mt-10 lg:mt-0 shadow-md flex items-center">
            <h1 className="text-[19px] lg:text-[20px] font-bold text-center mx-auto w-48 mr-7 text-[#1624A3]">
              Payment Point Aggregator & PPOB
            </h1>
          </div>
          <div className="w-[401px] h-[91px] bg-white rounded-xl shadow-md mt-10 flex items-center">
            <img
              className=" lg:w-[65px] object-cover lg:h-[65px] ml-5 "
              src={s4}
              alt=""
            />
            <h1 className="text-[19px] lg:text-[20px] font-bold text-center mx-auto mr-4 text-[#1624A3]">
              Banking & Cash Management
            </h1>
          </div>
          <div className="w-[351px] h-[91px] bg-white rounded-xl shadow-md mt-10 flex items-center">
            <img
              className="lg:w-[65px] object-cover lg:h-[65px] w-[50px] h-[50px] ml-7"
              src={s5}
              alt=""
            />
            <h1 className="text-[19px] lg:text-[20px] font-bold text-center mx-auto text-[#1624A3] mr-7">
              Courier & Logistics
            </h1>
          </div>
        </div>
      </div>
      <div className="w-full h-full">
        <h2 className="text-xl  mt-24 text-[#0082C9] text-center">
          <b>Why Choose Us</b>
        </h2>
      </div>
      <div className="w-[500px] lg:w-[1200px] flex flex-wrap mx-auto h-full justify-around">
        <div className="bg-white w-[345px] h-[91px] rounded-xl shadow-md mt-10 flex items-center">
          <h1 className="text-[20px] font-bold text-center mx-auto mr-5 text-[#1624A3]">
            10+ years of experience
          </h1>
          <img className="absolute ml-8 object-cover " src={vector1} alt="" />
        </div>
        <div className="bg-white w-[345px] h-[91px] rounded-xl shadow-md mt-10 flex items-center">
          <h1 className="text-[20px] font-bold text-center mx-auto mr-5 text-[#1624A3]">
            Cloud-based integration
          </h1>
          <img className="absolute  object-cover ml-8" src={vector2} alt="" />
        </div>
        <div className="bg-white w-[315px] h-[91px] rounded-xl shadow-md mt-10 flex items-center">
          <h1 className="text-[20px] font-bold text-center mx-auto mr-5 text-[#1624A3]">
            Rapid development
          </h1>
          <img className="absolute object-cover ml-8" src={vector3} alt="" />
        </div>
      </div>
      <div className="text-center mt-16">
        <h2 className="text-xl mb-7 text-[#0082C9]">
          <b>Our Awards</b>
        </h2>
        <h1 className="text-5xl mb-7 text-[#1624A3]">
          <b>
            <span className="text-[#265BE1]">1st</span> Winner
          </b>
        </h1>
        <h2 className="text-[#265BE1] text-lg">
          <b>of Google x BRI Indonesia Hackaton 2020</b>
        </h2>
      </div>
      <div className="flex justify-around mt-10 w-full h-[400px]">
        <img
          className="sm:w-[675px] object-cover sm:h-[418px] w-[250px] h-[400px]"
          src={prop}
          alt="/"
        />
        <img
          className="sm:w-[675px]  object-cover sm:h-[418px] w-[250px] h-[400px]"
          src={prop1}
          alt="/"
        />
      </div>
      <div className="text-center mt-16">
        <h2 className="text-xl mb-7 text-[#0082C9]">
          <b>Our Products</b>
        </h2>
      </div>

      <div className="sm:bg-white sm:w-[1349px] sm:h-[628px] mx-auto w-[500px] h-[1000px] sm:mb-0 mb-40 ">
        <div className="sm:w-[730px] sm:h-[508px] w-[500px] h-[500px] absolute sm:ml-5 ">
          <h1 className=" text-5xl sm:text-6xl ml-2 sm:ml-10 mt-10 text-[#1624A3]">
            <span className="text-[#265BE1]">PesantrenQu,</span> a Digital
            Pesantren for the era of 4.0
          </h1>
          <p className=" ml-5 sm:ml-10 mt-10 text-[#265BE1] text-lg">
            PesantrenQu is our product dedicated for Pesantren & under pilot in
            PesantrenQu that is aimmed to be the super apps of services within
            Pesantren ecosystem - be it cashless payment, edu bill payment,
            learning management system, and pesantrens' ERP for their business
            units.
          </p>
          <div>
            <button className="w-[143px] h-[43px] text-white bg-[#1624A3] rounded-lg mt-10 ml-4 sm:ml-12 cursor-pointer">
              <b>
                <a href="https://play.google.com/store/apps/details?id=id.siesta.app.madrasahqu.alyasini"></a>
                LEARN MORE
              </b>
            </button>
          </div>
        </div>
        <div className=" sm:w-[600px] sm:h-[500px] sm:ml-[800px] absolute sm:mt-0 mt-[500px] ">
          <img className="w-[674px] h-[628px] object-cover " src={AlyasinQu} alt="/" />
        </div>
      </div>

      <div className=" w-[500px] h-[1000px] sm:bg-white sm:w-[1349px] sm:h-[628px] mx-auto mt-32 ">
        <div className=" w-[450px] h-[500px] sm:w-[730px] sm:h-[508px] absolute ml-5 ">
          <h1 className=" text-5xl sm:text-6xl ml-0 sm:ml-10 mt-10 text-[#1624A3]">
            <span className="text-[#265BE1]">RetailQu,</span> your
            transformative retail toolkit beyond a retail business
          </h1>
          <p className="ml-4 sm:ml-10 mt-10 text-[#265BE1]">
            RetailQu is a solution for retail businesses to scale up and
            transform them to be a new retail 4.0 through integration with
            technology in supply chain, delivery, and finance.
          </p>
          <div>
            <button className="w-[143px] h-[43px] text-white bg-[#1624A3] rounded-lg mt-10 ml-3 sm:ml-12 cursor-pointer">
              <b>
                <a href="https://play.google.com/store/apps/details?id=id.siesta.app.madrasahqu.alyasini"></a>
                LEARN MORE
              </b>
            </button>
          </div>
        </div>
        <div className=" sm:w-[600px] sm:h-[500px] sm:ml-[800px] absolute sm:mt-0 mt-[500px]">
          <img className="w-[674px] h-[628px] object-cover" src={Retailqu} alt="/" />
        </div>
      </div>

      <div className=" w-[500px] h-[1000px] sm:bg-white sm:w-[1349px] sm:h-[628px] mx-auto mt-32 ">
        <div className=" w-[450px] h-[500px] sm:w-[730px] sm:h-[508px] absolute ml-5 ">
          <h1 className=" text-5xl sm:text-6xl ml-0 sm:ml-10 mt-10 text-[#1624A3]">
            <span className="text-[#265BE1]">CampusQu,</span>Streamline campus
            management with dynamic features
          </h1>
          <p className="ml-1 sm:ml-10 mt-10 text-[#265BE1]">
            CampusQu is our flagship complete and secure integrated academic
            management solution to facilitate university governance and PDDIKTI
            reporting.
          </p>
          <div>
            <button className="w-[143px] h-[43px] text-white bg-[#1624A3] rounded-lg mt-10 ml-3 sm:ml-12 cursor-pointer">
              <b>
                <a href="https://play.google.com/store/apps/details?id=id.siesta.app.madrasahqu.alyasini"></a>
                LEARN MORE
              </b>
            </button>
          </div>
        </div>
        <div className=" sm:w-[800px] sm:h-[500px] sm:ml-[710px] absolute sm:mt-0 mt-[500px]">
          <img
            className="w-[675px] h-[515px] object-cover"
            src={Campusqu}
            alt="/"
          />
        </div>
      </div>

      <div className="flex items-end w-full min-h-screen bg-white">
        <footer className="w-full text-white font-bold bg-[#1624A3] body-font">
          <div className="container flex flex-col flex-wrap px-5 py-24 mx-auto md:items-center lg:items-start md:flex-row md:flex-no-wrap">
            <div className="flex-shrink-0 w-64 mx-auto text-center md:mx-0 md:text-left">
              <h1 className="text-xl">SIESTA</h1>
              <p className="mt-2 text-sm text-[white]">is a brand of</p>
              <p className="mt-2 text-sm text-[white]">
                PT. Solusi Infotech Semesta Indonesia
              </p>
              <p className="mt-2 text-sm text-[white]">
                PT. Solusi Infotech Nuswantara
              </p>
              <div className="mt-4">
                <span className="inline-flex justify-center mt-2 sm:ml-auto sm:mt-0 sm:justify-start">
                  <a className="text-white cursor-pointer">
                    <svg
                      fill="currentColor"
                      strokeLinecap="round"
                      strokeLinejoin="round"
                      strokeWidth="2"
                      className="w-5 h-5"
                      viewBox="0 0 24 24"
                    >
                      <path d="M18 2h-3a5 5 0 00-5 5v3H7v4h3v8h4v-8h3l1-4h-4V7a1 1 0 011-1h3z"></path>
                    </svg>
                  </a>
                  <a className="ml-3 text-white cursor-pointer">
                    <svg
                      fill="none"
                      stroke="currentColor"
                      strokeLinecap="round"
                      strokeLinejoin="round"
                      strokeWidth="2"
                      className="w-5 h-5"
                      viewBox="0 0 24 24"
                    >
                      <rect
                        width="20"
                        height="20"
                        x="2"
                        y="2"
                        rx="5"
                        ry="5"
                      ></rect>
                      <path d="M16 11.37A4 4 0 1112.63 8 4 4 0 0116 11.37zm1.5-4.87h.01"></path>
                    </svg>
                  </a>
                  <a className="ml-3 text-white cursor-pointer">
                    <svg
                      fill="currentColor"
                      stroke="currentColor"
                      strokeLinecap="round"
                      strokeLinejoin="round"
                      strokeWidth="0"
                      className="w-5 h-5"
                      viewBox="0 0 24 24"
                    >
                      <path
                        stroke="none"
                        d="M16 8a6 6 0 016 6v7h-4v-7a2 2 0 00-2-2 2 2 0 00-2 2v7h-4v-7a6 6 0 016-6zM2 9h4v12H2z"
                      ></path>
                      <circle cx="4" cy="4" r="2" stroke="none"></circle>
                    </svg>
                  </a>
                </span>
              </div>
            </div>
            <div className="flex flex-wrap  mt-10 -mb-10 text-center md:pl-20 md:mt-0 md:text-left">
              <div className="w-full px-30 lg:w-60 md:w-1/2">
                <h2 className="mb-3 text-xl font-medium tracking-widest text-white uppercase title-font ">
                  Address
                </h2>
                <nav className="mb-10 list-none">
                  <li className="mt-3">
                    <a className="text-white cursor-pointer">
                      Jl. Ciganitri Pertanian Lengkong, Kec. Bojongsoang,
                      Bandung, Jawa Barat 40287
                    </a>
                  </li>
                </nav>
              </div>
              <div className="w-full px-32 lg:w-1/2 md:w-1/2">
                <h2 className="mb-3 text-xl font-medinpm um tracking-widest text-white uppercase title-font">
                  Bussines
                </h2>
                <nav className="mb-10 list-none">
                  <li className="mt-3">
                    <a className="text-white cursor-pointer">
                      business@siesta.id
                    </a>
                  </li>
                </nav>
              </div>
            </div>
          </div>
        </footer>
      </div>

      <div className="pb-[500px]"></div>
    </>
  );
}

export default App;
